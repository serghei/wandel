package notify

import (
	"fmt"
	"maps"
	"os"
	"slices"
	"strings"
	"time"
)

const errorPrefix = "Error:"

func Starting() {
	fmt.Printf("%s Watching files...\n", prefix())
}

func Reloading() {
	fmt.Printf("%s Reloading %s due to modification\n", prefix(), underline("configuration file"))
}

func Changes(filePath string) {
	if filePath != "" {
		fmt.Printf("%s Changes in %s\n", prefix(), underline(filePath))
	}
}

func Execution(command string) {
	fmt.Printf("%s Executing %s\n", prefix(), underline(command))
}

func ErrNotFound(filePath string) {
	fmt.Fprintf(os.Stderr, "%s %s Can't find %s\n", prefix(), errorPrefix, underline(filePath))
}

func ErrConfigExists() {
	fmt.Fprintf(os.Stderr, "%s %s configuration file already exists\n", prefix(), errorPrefix)
}

func ErrReadDir(dirPath string) {
	fmt.Fprintf(os.Stderr, "%s %s Failed to read %s\n", prefix(), errorPrefix, underline(dirPath))
}

func ErrWatcherAdd(filePath string) {
	fmt.Fprintf(os.Stderr, "%s %s Failed to add %s to watcher\n", prefix(), errorPrefix, filePath)
}

func ErrSystem(err error) {
	fmt.Fprintf(os.Stderr, "%s System error: %s\n", prefix(), err.Error())
}

// FATAL

func FatalDuplicateUse(table, use string) {
	fmt.Fprintf(os.Stderr, "%s %s Table %s inherits common %s multiple times. This is caused by a configuration error in your configuration file\n",
		prefix(),
		errorPrefix,
		underline(table),
		underline(use),
	)
	os.Exit(1)
}

func FatalDuplicateIgnoreGlobal(table string) {
	fmt.Fprintf(os.Stderr, "%s %s Table %s inherits different values for the property %s. Inherited values: [ true, false ]. This is caused by a configuration error in your configuration file\n",
		prefix(),
		errorPrefix,
		underline(table),
		underline("ignore_global"),
	)
	os.Exit(1)
}

func FatalDuplicateRecursive(table string) {
	fmt.Fprintf(os.Stderr, "%s %s Table %s inherits different values for the property %s. Inherited values: [ true, false ]. This is caused by a configuration error in your configuration file\n",
		prefix(),
		errorPrefix,
		underline(table),
		underline("recursive"),
	)
	os.Exit(1)
}

func FatalDuplicatePattern(table string, vals map[string]bool) {
	fmt.Fprintf(os.Stderr, "%s %s Table %s inherits different values for the property %s. Inherited values: [ %s ]. This is caused by a configuration error in your configuration file\n",
		prefix(),
		errorPrefix,
		underline(table),
		underline("pattern"),
		strings.Join(slices.Collect(maps.Keys(vals))[:], ", "),
	)
	os.Exit(1)
}

func FatalPatternInvalid(pattern string) {
	fmt.Fprintf(os.Stderr, "%s %s Failed to parse provided pattern %s\n", prefix(), errorPrefix, underline(pattern))
}

func FatalWatcher() {
	fmt.Fprintf(os.Stderr, "%s %s Failed to initialize watcher. Terminating\n", prefix(), errorPrefix)
	os.Exit(1)
}

func FatalWatcherEmpty() {
	fmt.Fprintf(os.Stderr, "%s There are no paths to watch. Terminating.\n", prefix())
	os.Exit(1)
}

func FatalWatcherClose() {
	fmt.Fprintf(os.Stderr, "%s The watcher was closed for unexpected reasons. Terminating.\n", prefix())
	os.Exit(1)
}

// UTIL

func prefix() string {
	return fmt.Sprintf("[WURM] - [%s]", time.Now().Format("15:04:05"))
}

func underline(text string) string {
	return fmt.Sprintf("\033[4m%s\033[0m", text)
}
