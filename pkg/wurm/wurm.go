// Package wurm implements the required functions to use Wurm as a library.
package wurm

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"slices"
	"strings"

	"codeberg.org/serghei/wurm/internal/config"
	"codeberg.org/serghei/wurm/internal/notify"
	"github.com/fsnotify/fsnotify"
)

type application struct {
	watcher   *fsnotify.Watcher
	filePaths []string
	ignored   []string
	functions []func()
	config    *config.Config

	activeRunner string
}

// New creates and returns a new application instance.
// It uses the global configuration if a configuration file is'nt present.
func New() *application {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		notify.FatalWatcher()
	}

	app := &application{
		watcher:   watcher,
		filePaths: []string{},
		functions: []func(){},
		ignored:   []string{},
		config:    config.ReadFile(),
	}

	if _, err := os.Stat(config.ConfigFile); !errors.Is(err, os.ErrNotExist) {
		app.filePaths = append(app.filePaths, config.ConfigFile)
	}

	return app
}

// Run creates a new application instance.
// It uses the table argument to read the associated table from the configuration file
// and starts watching the table's paths for changes.
func Run(table string) {
	app := New()
	app.activeRunner = table
	app.prepareRunner()

	notify.Starting()
	app.watch()
}

// Watch adds the filePaths to the watchers paths and starts watching the paths for changes.
// Must be called last, after providing all necessary data to the application instance.
func (app *application) Watch(filePaths ...string) {
	app.filePaths = append(app.filePaths, filePaths...)
	slices.Sort(app.filePaths)
	app.filePaths = slices.Compact(app.filePaths)

	app.watcherInit(app.filePaths)
	app.validateApp()
	notify.Starting()
	app.watch()
}

// AddFunctions adds functions to the list of tasks to execute when changes occur.
func (app *application) AddFunctions(functions ...func()) {
	for _, function := range functions {
		app.functions = append(app.functions, function)
	}
}

// AddCommands adds commands to the list of tasks to execute when changes occur.
func (app *application) AddCommands(commands ...string) {
	for _, command := range commands {
		command = strings.TrimSpace(command)
		if len(command) > 0 {
			app.addCommand(command)
		}
	}
}

func (app *application) addCommand(command string) {
	function := func() {
		notify.Execution(command)
		cmds := strings.Split(command, "&&")
		for _, cmd := range cmds {
			cmd = strings.TrimSpace(cmd)
			split := strings.Split(cmd, " ")
			exe := exec.Command(split[0], split[1:]...)
			exe.Stdout = os.Stdout
			exe.Stderr = os.Stderr
			exe.Run()
		}
	}

	app.functions = append(app.functions, function)
}

// Ignore adds filePaths to the list of paths to not watch for changes.
func (app *application) Ignore(filePaths ...string) {
	for _, filePath := range filePaths {
		if filePath != "" && !slices.Contains(app.ignored, prefixPath(filePath)) {
			app.ignored = append(app.ignored, prefixPath(filePath))
		}
	}

	app.ignored = slices.Compact(app.ignored)
	app.ignored = slices.Clip(app.ignored)
}

// SetRecursive configures the application to recursively watch directories based on recursive.
func (app *application) SetRecursive(recursive bool) {
	config.Recursive = recursive
}

// SetPattern sets the pattern that watched paths must match.
func (app *application) SetPattern(pattern string) {
	config.Pattern = pattern
}

func (app *application) prepareRunner() {
	global := app.config.Global
	common := app.config.Common
	runner := app.config.Runner[app.activeRunner]

	overwrittenRecursive := false
	overwrittenPattern := false

	if app.isDefined("runner", app.activeRunner, "recursive") {
		overwrittenRecursive = true
		config.Recursive = runner.Recursive
	}
	if app.isDefined("runner", app.activeRunner, "pattern") {
		overwrittenPattern = true
		config.Pattern = runner.Pattern
	}

	if !app.ignoreGlobal() {
		if app.isDefined("global", "recursive") && !overwrittenRecursive {
			config.Recursive = global.Recursive
		}
		if app.isDefined("global", "pattern") && !overwrittenPattern {
			config.Pattern = global.Pattern
		}

		runner.Append(&global)
	}

	used := map[string]bool{}
	valuesRecursive := map[bool]bool{}
	valuesPattern := map[string]bool{}
	for _, use := range runner.Use {
		useTable := common[use]
		if used[use] {
			notify.FatalDuplicateUse(app.activeRunner, use)
		}

		used[use] = true
		runner.Use = append(runner.Use, useTable.Use...)

		if app.isDefined("common", use, "recursive") && !overwrittenRecursive {
			if !valuesRecursive[useTable.Recursive] &&
				len(valuesRecursive) > 0 {
				notify.FatalDuplicateRecursive(app.activeRunner)
			}
			valuesRecursive[useTable.Recursive] = true
			config.Recursive = useTable.Recursive
		}
		if app.isDefined("common", use, "pattern") && !overwrittenPattern {
			if !valuesPattern[useTable.Pattern] &&
				len(valuesPattern) > 0 &&
				useTable.Pattern != "" {
				valuesPattern[useTable.Pattern] = true
				notify.FatalDuplicatePattern(app.activeRunner, valuesPattern)
			}
			valuesPattern[useTable.Pattern] = true
			config.Pattern = useTable.Pattern
		}

		runner.Append(&useTable)
	}

	app.filePaths = append(app.filePaths, runner.Paths...)
	slices.Sort(app.filePaths)
	app.filePaths = slices.Compact(app.filePaths)

	app.initApp(runner.Ignore, app.filePaths, runner.Commands)
	app.validateApp()
}

func (app *application) ignoreGlobal() (ignore bool) {
	global := app.config.Global
	common := app.config.Common
	runner := app.config.Runner[app.activeRunner]

	if app.isDefined("runner", app.activeRunner, "ignore_global") {
		return runner.IgnoreGlobal
	}

	if app.isDefined("global", "ignore_global") {
		ignore = global.IgnoreGlobal
	}

	changedIgnoreGlobal := false
	for _, use := range runner.Use {
		if !app.isDefined("common", use, "ignore_global") {
			continue
		}
		if changedIgnoreGlobal {
			notify.FatalDuplicateIgnoreGlobal(app.activeRunner)
		}
		changedIgnoreGlobal = true
		ignore = common[use].IgnoreGlobal
	}

	return ignore
}

func (app *application) isDefined(keys ...string) bool {
	return app.config.Meta.IsDefined(keys...)
}

func (app *application) initApp(ignored, paths, commands []string) {
	app.Ignore(ignored...)
	app.AddCommands(commands...)
	app.watcherInit(paths)
}

func (app *application) resetApp() {
	app.watcher.Close()
	app.ignored = []string{}
	app.functions = []func(){}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		notify.FatalWatcher()
	}

	app.watcher = watcher
	app.config = config.ReadFile()

	global := app.config.Global
	app.initApp(global.Ignore, global.Paths, global.Commands)

	app.AddCommands(config.Command)
	app.Ignore(strings.Split(config.Ignore, " ")...)
	app.watcherInit(app.filePaths)

	if app.activeRunner != "" {
		app.prepareRunner()
	}
}

func (app *application) validateApp() {
	if app.watcher.WatchList() == nil {
		notify.FatalWatcherClose()
	} else if len(app.watcher.WatchList()) == 0 {
		notify.FatalWatcherEmpty()
	}
}

func (app *application) watcherInit(entries []string) {
	if config.Pattern != "" {
		rgx, err := regexp.Compile(config.Pattern)
		if err != nil {
			notify.FatalPatternInvalid(config.Pattern)
		}

		config.PatternCompiled = rgx
	}

	for _, entryPath := range entries {
		if slices.Contains(app.ignored, entryPath) {
			continue
		}

		stat, err := os.Stat(entryPath)
		if errors.Is(err, os.ErrNotExist) {
			notify.ErrNotFound(entryPath)
			continue
		}

		if stat.IsDir() {
			app.addWatcher(entryPath)
			if config.Recursive {
				app.watcherAddEntries(entryPath, getEntries(entryPath))
			}
		} else {
			parentPath := filepath.Dir(entryPath)
			for _, entry := range getEntries(parentPath) {
				if slices.Contains(entries, entry.Name()) {
					continue
				}

				app.Ignore(entry.Name())
			}

			app.addWatcher(parentPath)
		}
	}
}

func (app *application) watcherAddEntries(currentPath string, entries []fs.DirEntry) {
	parentPath := fmt.Sprintf("%s%c", currentPath, os.PathSeparator)
	if currentPath == "." {
		parentPath = ""
	}

	for _, entry := range entries {
		entryPath := parentPath + entry.Name()
		if slices.Contains(app.ignored, entryPath) {
			continue
		}

		if entry.IsDir() {
			if config.Recursive {
				app.watcherAddEntries(entryPath, getEntries(entryPath))
			}
		} else if config.PatternCompiled != nil && !config.PatternCompiled.MatchString(entryPath) {
			continue
		}

		app.addWatcher(entryPath)
	}
}

func (app *application) watch() {
	for {
	breakIgnored:
		select {
		case err, ok := <-app.watcher.Errors:
			if !ok {
				notify.FatalWatcherClose()
			}
			notify.ErrSystem(err)
		case event, ok := <-app.watcher.Events:
			if !ok {
				notify.FatalWatcherClose()
			}

			if event.Has(fsnotify.Write) {
				fileName := prefixPath(event.Name)
				for _, ignored := range app.ignored {
					if fileName == ignored ||
						strings.HasPrefix(fileName, ignored) ||
						config.PatternCompiled != nil && !config.PatternCompiled.MatchString(fileName) {
						break breakIgnored
					}
				}

				if fileName == prefixPath(config.ConfigFile) {
					app.resetApp()
				}

				app.executeFunctions(event.Name)
			}
		}
	}
}

func (app *application) executeFunctions(filePath string) {
	if filePath == prefixPath(config.ConfigFile) {
		notify.Reloading()
		return
	}

	notify.Changes(filePath)

	for _, function := range app.functions {
		function()
	}
}

func (app *application) addWatcher(entryPath string) {
	if err := app.watcher.Add(entryPath); err != nil {
		notify.ErrWatcherAdd(entryPath)
	}
}

func prefixPath(name string) string {
	if !strings.HasPrefix(name, fmt.Sprintf(".%c", os.PathSeparator)) {
		return fmt.Sprintf(".%c%s", os.PathSeparator, name)
	}

	return name
}

func getEntries(filePath string) []fs.DirEntry {
	entries, err := os.ReadDir(filePath)
	if err != nil {
		notify.ErrReadDir(filePath)
		return []fs.DirEntry{}
	}

	return entries
}
